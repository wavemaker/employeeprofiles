var _WM_APP_PROPERTIES = {
  "activeTheme" : "mobile",
  "defaultLanguage" : "en",
  "displayName" : "EmployeeProfiles",
  "homePage" : "Main",
  "name" : "EmployeeProfiles",
  "platformType" : "MOBILE",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};